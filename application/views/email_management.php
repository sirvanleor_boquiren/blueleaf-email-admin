<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Email Management
					</header>
					<div class="panel-body">
						<div class="table-responsive">
						<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<br>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <br>
					        <?php endif; ?>
					        <div class="row" style="width:100%">
					        	<form name="filters" id="filters" method="get">
						        	<div class="col-lg-4">
							        	<div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
		                                  <input type="text" class="form-control dpd1" name="from" placeholder="Start Inquiry Date" <?php echo isset($_GET['from']) && $_GET['from'] != "" ? 'value="'.date("m/d/Y", strtotime($_GET['from'])).'"' : "";  ?>>
		                                  <span class="input-group-addon">To</span>
		                                  <input type="text" class="form-control dpd2" name="to" placeholder="End Inquiry Date" <?php echo isset($_GET['to']) && $_GET['to'] != "" ? 'value="'.date("m/d/Y", strtotime($_GET['to'])).'"' : "";  ?>>
		                              	</div>
	                            	</div>
	                            	<div class="col-lg-3">
	                            		<input type="submit" class="btn btn-primary" value="Filter">
	                            		<a href="<?php echo base_url("Emails/Manage"); ?>" class="btn btn-warning">Clear</a>
	                            		<a href="<?php echo $csv_export_link; ?>" class="btn btn-info">Export</a>
	                            	</div>
					        	</form>
					        	
					        </div>
					        <br>
					        <div class="alert alert-info fade in">
				              	<button data-dismiss="alert" class="close close-sm" type="button">
				                  	<i class="fa fa-times"></i>
				              	</button>
				             	<strong>Heads up!</strong><br>All <strong>Dates</strong> are in format <strong>(MM/DD/YYYY)</strong>
				          	</div>
                          <table class="table table-bordered">
                              <thead>
								<tr>
									<?php if($is_super): ?>
									<th>Type</th>
									<?php endif; ?>
									<th>Name</th>
									<th>Email</th>
									<th>Landline</th>
									<th>Mobile</th>
									<th>Venue</th>
									<th>Event Date</th>
									<th>Event Time</th>
									<th>Guest #</th>
									<th>Event Type</th>
									<th style="width:30%">Message</th>
									<th>Inquiry Date</th>
								</tr>
							</thead>
                            <tbody>
                            <?php if(isset($all_emails) && $all_emails != null): ?>
							<?php foreach ($all_emails as $email) { ?>
							 	<tr>
							 		<?php if($is_super): ?>
							 		<td>
							 			<?php echo get_type_desc($email->type); ?>
							 		</td>
							 		<?php endif; ?>
							 		<td><?php echo $email->name; ?></td>
							 		<td><?php echo $email->email; ?></td>
							 		<td><?php echo $email->landline != "" ? $email->landline : "N/A"; ?></td>
							 		<td><?php echo $email->mobile != "" ? $email->mobile : "N/A"; ?></td>
							 		<td><?php echo $email->venue != "" ? $email->venue : "N/A"; ?></td>
							 		<td><?php echo $email->event_date != "" ? date("m/d/Y", strtotime($email->event_date)) : "N/A"; ?></td>
							 		<td><?php echo $email->event_time != "" ? $email->event_time : "N/A"; ?></td>
							 		<td><?php echo $email->guest_num != "" ? $email->guest_num : "N/A"; ?></td>
							 		<td><?php echo $email->event_type != "" ? $email->event_type : "N/A"; ?></td>
							 		<td><?php echo nl2br($email->message); ?></td>
							 		<td><?php echo date("m/d/Y", strtotime($email->date_sent)); ?></td>
								</tr>
							<?php } ?>
						<?php else:?>
							<tr>
								<td colspan="<?php echo $td_colspan; ?>"><center>No records found.</center></td>
							</tr>
						<?php endif; ?>
							</tbody>
							<tfoot>
								<tr>
									<?php if($is_super): ?>
									<th>Type</th>
									<?php endif; ?>
									<th>Name</th>
									<th>Email</th>
									<th>Landline</th>
									<th>Mobile</th>
									<th>Venue</th>
									<th>Event Date</th>
									<th>Event Time</th>
									<th>Guest #</th>
									<th>Event Type</th>
									<th>Message</th>
									<th>Inquiry Date</th>
								</tr>
							</tfoot>
                          </table>
                          <div class="text-center">
	                          <ul class="pagination">
	                          	<?php echo $pagination; ?>
	                          </ul>
                          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->

<!-- Modal: Geolocation / Start -->
<div class="modal fade" id="modal_map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Geo/Map Location</h4>
          </div>
          <div class="modal-body" id="modal_body">
          	 <div class="alert alert-info fade in">
	              <button data-dismiss="alert" class="close close-sm" type="button">
	                  <i class="fa fa-times"></i>
	              </button>
	              <strong>Heads up!</strong><br><strong>S</strong> = Store Location / <strong>A</strong> = Analyst Location
	          </div>
            <div id="map" class="map-audit"></div>
          </div>
          <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
          </div>
      </div>
  </div>
</div>
<!-- Modal: Geolocation / End -->


<!-- Modal: Assign to an existing Store / Start -->
<div class="modal fade" id="modal_assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Assign Store</h4>
          </div>
          <div class="modal-body" id="modal_body">
          	<form class="form-horizontal tasi-form" method="POST" action="<?php echo base_url("Reports/assignStore"); ?>">
          		<div class="form-group">
          			<label class="col-sm-3 col-sm-3 control-label">
          				Stores
          			</label>
          			<div class="col-sm-9">
          				<select name="s_id" id="assign_store_id" class="form-control">
		        			<option value="">Select Store</option>
		        			<?php foreach ($store_list as $store_obj) { ?>
		        				<option value="<?php echo $store_obj->s_id ?>"><?php echo $store_obj->store_name; ?></option>
		        			<?php } ?>
		        		</select>
		        		<input type="hidden" name="sa_id" id="assign_sa_id">
          			</div>
          		</div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
          	<button type="submit" class="btn btn-success">Submit</button>
          </div>
       	</form>
      </div>
  </div>
</div>
<!-- Modal: Assign to an existing Store / End -->


<!-- Scripts -->
<script>
</script>