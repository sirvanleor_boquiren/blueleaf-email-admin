<?php 
$sub_menu = array();
$sub_menu["users"] = array("Users/App", "Users/Admin");
$sub_menu["reports"] = array("Reports/Audit", "Reports/Store");

?>
<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">

    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <li>
        <a <?php echo uri_string() == "Emails/Manage" ? "class='active'" : ""; ?> href="<?php echo base_url('Emails/Manage'); ?>">
          <i class="fa fa-envelope"></i>
          <span>Email Management</span>
        </a>
      </li>
      <?php if(isset($_SESSION['is_super']) && $_SESSION['is_super'] == 1): ?>
      <li class="sub-menu">
        <a <?php echo uri_string() == "Users/Manage" || strpos(uri_string(), "Users/Edit/") !== false ? "class='active'" : ""; ?> href="<?php echo base_url('Users/Manage'); ?>" >
          <i class="fa fa-users"></i>
          <span>Manage Users</span>
        </a>
      </li>
    <?php endif; ?>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->