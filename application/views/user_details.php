<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-8">
				<section class="panel">
					<header class="panel-heading">
						<?php echo $panel_header; ?>
					</header>
					<div class="panel-body">
						<form id="edit_user" name="edit_user" class="form-horizontal tasi-form" method="POST" action="<?php echo $form_action; ?>">
							<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <?php endif; ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Fullname <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="fullname" id="fullname" value="<?php echo isset($user) ? $user->fullname : ""; ?>" placeholder="Enter Firstname" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Email <span class="required">*</span></label>
								<div class="col-sm-10">
									<input class="form-control" type="email" name="email" id="email" value="<?php echo isset($user) ? $user->email : ""; ?>" placeholder="Enter Email" onkeyup="check_email();" required>
									<p class="help-block" id="invalid_email" style="color:red; display:none;">Email is already registered</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Password <?php echo !isset($user) ? '<span class="required">*</span>' : "";  ?></label>
								<div class="col-sm-10">
									<input class="form-control" type="password" name="password" id="password" value="" onkeyup="check_pass();" <?php echo isset($user) ? "placeholder='Leave blank if not going to change password.'" : "required"; ?>>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Confirm Password <?php echo !isset($user) ? '<span class="required">*</span>' : "";  ?></label>
								<div class="col-sm-10">
									<input class="form-control" type="password" name="con_password" id="con_password" value="" onkeyup="check_pass();" <?php echo isset($user) ? "placeholder='Leave blank if not going to change password.'" : "required"; ?>>
									<p class="help-block" id="password_match" style="color:red; display:none;">Password does not match</p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Venue <span class="required">*</span></label>
								<div class="col-sm-10">
									<select name="type" id="type" class="form-control" required>
										<option value="">Select Venue Assignment</option>
										<?php foreach (get_all_venues() as $key => $desc) { ?>
										<option value="<?php echo $key; ?>" <?php echo isset($user) && $key == $user->type ? "selected" : ""; ?>><?php echo $desc; ?></option>
										<?php } ?>
										<option value="0">Super Admin</option>
									</select>
								</div>
							</div>

							<?php if(isset($user)): ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status <span class="required">*</span></label>
								<div class="col-sm-10">
									<select name="is_deleted" id="is_deleted" class="form-control" required>
										<option value="">Select Status</option>
										<option value="0" <?php echo isset($user) && $user->is_deleted == 0 ? "selected" : ""; ?>>Active</option>
										<option value="1" <?php echo isset($user) && $user->is_deleted == 1 ? "selected" : ""; ?>>In-Active</option>
									</select>
								</div>
							</div>
						<?php endif; ?>

							<div class="pull-right">
								<?php if(isset($user)): ?>
								<input type="hidden" name="u_id" id="u_id" value="<?php echo $user->u_id; ?>">
							<?php endif; ?>
								<a href="<?php echo base_url($back_page); ?>" class="btn btn-info btn-shadow">< Back to Table</a>
								<button type="submit" id="submit_edit" class="btn btn-success btn-shadow">Submit</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->
<?php if(!isset($user)): ?>
<script type="text/javascript">
function check_email()
{
	$.ajax({
	    url:"<?php echo base_url("Ajax/checkEmail"); ?>",
	    type: "POST",
	    data: {email: $('#email').val() },
	    success:function(data){ // data is the id of the row
	      if(data == true)
	      {
	      	$('#submit_edit').prop("disabled", "true");
	      	$('#invalid_email').attr("style", "color:red; display:block");
	      }
	      else
	      {
	      	$('#submit_edit').removeAttr("disabled");
	      	$('#invalid_email').attr("style", "color:red; display:none");
	      }
	    },
	    error: function(e){
	      console.log(e);
	    }
	  });
}

function check_pass()
{
	var p = $('#password').val();
	var c = $('#con_password').val();
	if(p != "" && c != "")
	{
		if(p != c)
		{
			$('#submit_edit').prop("disabled", "true");
			$('#password_match').attr("style", "color:red; display:block");
		}
		else
		{
			$('#submit_edit').removeAttr("disabled");
			$('#password_match').attr("style", "color:red; display:none");
		}
	}
}

</script>
<?php endif; ?>