<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Users Management
					</header>
					<div class="panel-body">
						<div class="table-responsive">
						<div class="pull-right">
							<a href="<?php echo base_url($add_link); ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>&nbsp;Add User</a><br><br>
						</div>
						<br>
						<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <?php endif; ?>
					      <br>
                          <table class="table table-bordered">
                              <thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Venue</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
							<?php 
							if(count($all_users) > 0)
							{
								foreach ($all_users as $user_arr) { ?>
								<tr>
									<td><?php echo $user_arr->fullname; ?></td>
									<td><?php echo $user_arr->email; ?></td>
									<td><?php echo get_type_full_desc($user_arr->type); ?></td>
									<td><?php echo $user_arr->is_deleted == "1" ? "In-active" : "Active"; ?></td>
									<td>
										<a href="<?php echo base_url("Users/Edit/").$user_arr->u_id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
									</td>
								</tr>
								<?php
								}
							}
							else
							{ ?>
							<tr>
								<td colspan="6"><center>No records found.<center></td>
							</tr>
							<?php
							} ?>
							</tbody>
							<tfoot>
								<tr>
								  <th>Name</th>
									<th>Email</th>
									<th>Venue</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</tfoot>
                          </table>
                          <div class="text-center">
	                          <ul class="pagination">
	                          	<?php echo $pagination; ?>
	                          </ul>
                          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->