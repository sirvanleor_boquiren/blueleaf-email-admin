<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        $this->load->model('users_model');

        #Load needed libraries not in autoload
        $this->load->library('pagination');

        # Set include array for header and footer
        $this->include = array();
    }

    public function wrapper($body, $data = NULL) 
	{
		if (check_login($this->session)) 
		{
			$this->load->view('partials/header', $this->include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer', $this->include);
		}
		else {
			redirect(base_url());
		}
	}

	# DATA Wrapper Functions 

	public function Manage()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Users/Manage";
		$pag_conf['total_rows'] = $this->users_model->getTotalUsers();
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data["pagination"] = $this->pagination->create_links();

		# Get users
		$page_data['all_users'] = $this->users_model->getAllUsers();


		# Set Add user link
		$page_data["add_link"] = "Users/Add";

		# That's a wrap
		$this->wrapper("user_management", $page_data);
	}


	public function Edit($id)
	{
		# Get User Details
		$page_data['user'] = $this->users_model->getAccount($id);

		# Set Form action url
		$page_data['form_action'] = base_url("Users/editUser");

		# Set Back Page 
		$page_data['back_page'] = "Users/Manage";

		# Set Panel header
		$page_data['panel_header'] = "Edit User";

		# That's a wrap
		$this->wrapper("user_details.php", $page_data);
	}

	public function Add()
	{

		# Set Form action url
		$page_data['form_action'] = base_url("Users/addUser");

		# Set Back Page
		$page_data['back_page'] = "Users/Manage";

		# Set Panel header
		$page_data['panel_header'] = "Add User";

		# That's a wrap
		$this->wrapper("user_details.php", $page_data);
	}

	# POST Control Functions

	public function editUser()
	{
		$msg_data = array('alert_msg' => 'Something went wrong. Please try again or contact maintenance.', 'alert_color' => 'red');
		if($this->users_model->editUser($this->input->post()))
		{
			$msg_data = array('alert_msg' => 'User Successfully Updated', 'alert_color' => 'green');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('Users/Edit/'.$this->input->post("u_id")));
	}

	public function addUser($type = null)
	{
			$is_error = false;
			$required_post = array("fullname", "email", "password", "con_password", "type");
			foreach ($required_post as $post_key) {
				if(null !== $this->input->post($post_key))
				{
					if($this->input->post($post_key) == "")
					{
						$is_error = true;
						$msg = "Required fields not complete. Please complete all required fields.";
					}
				}
				else
				{
					$is_error = true;
					$msg = "Required fields not complete. Please complete all required fields.";
				}
			}

			if($this->input->post("password") != $this->input->post("con_password"))
			{
				$is_error = true;
				$msg = "Password and Confirm Password doest not match.";
			}

			if($is_error)
			{
				$msg_data = array("alert_msg" => $msg, "alert_color" => "red");
			}
			else
			{
				$msg_data = $this->users_model->addUser($this->input->post());
			}
			
			$this->session->set_flashdata($msg_data);
			redirect(base_url('Users/Add'));
	}

}
?>