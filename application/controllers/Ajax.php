<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        // $this->load->model("positions_model");
        $this->load->model("ajax_model");
        $this->load->model("store_model");
    }


    # User Management Forms
	public function checkEmail()
	{
		echo $this->ajax_model->checkExists($this->input->post(), "users", "u_id", "email");
	}
}
?>