<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        # Load all models
        $this->load->model("email_model");

        #Load needed libraries not in autoload
        $this->load->library('pagination');

        # Set include array for header and footer
        $this->include = array();
    }

    public function wrapper($body, $data = NULL) 
	{
		if (check_login($this->session)) 
		{
			$this->load->view('partials/header', $this->include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer', $this->include);
		}
		else {
			redirect(base_url());
		}
	}

	# DATA Wrapper Functions

	public function Manage()
	{
		# Ready css/js
		$this->include["responsive_table"] = true;

		# Set Export links
		$page_data["csv_export_link"] = base_url("Emails/auditExportCSV").concat_existing_get();

		# Get Pagination
		$pag_conf['base_url'] = base_url()."Emails/Manage";
		$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
		$pag_conf['total_rows'] = $this->email_model->getTotalEmails();
		$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
		$this->pagination->initialize($pag_conf);
		$page_data['pagination'] = $this->pagination->create_links();

		# Set if super admin
		$page_data['is_super'] = false;
		$page_data['td_colspan'] = 11;
		if($this->session->userdata['is_super'] == 1)
		{
			$page_data['is_super'] = true;
			$page_data['td_colspan'] = 12;
		}

		# Get All Audit
		$page_data["all_emails"] = $this->email_model->getAllEmails();

		$this->wrapper("email_management", $page_data);


	}

	public function auditExportCSV()
	{
		# Set Column Headers of CSV
		$column_headers = array("Name", "Email", "Landline", "Mobile", "Venue", "Event Date", "Event Time", "Guest #", "Event Type", "Message", "Inquiry Date");

		if(isset($_SESSION['is_super']) && $_SESSION['is_super'] == 1)
		{
			$column_headers[] = "Type";
		}
		$fp = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=email_report.csv');
		fputcsv($fp, $column_headers);


		# Get All Store Summary
		$export_data = $this->email_model->getAllEmails(true);
		foreach ($export_data as $export_arr) {
			$final_row = array(
				$export_arr->name,
				$export_arr->email,
				$export_arr->landline,
				$export_arr->mobile,
				$export_arr->venue,
				date("m/d/Y", strtotime($export_arr->event_date)),
				$export_arr->event_time,
				$export_arr->guest_num,
				$export_arr->event_type,
				$export_arr->message,
				date("m/d/Y", strtotime($export_arr->date_sent))
				);
			if(isset($_SESSION['is_super']) && $_SESSION['is_super'] == 1)
			{
				$final_row[] = get_type_desc($export_arr->type);
			} 
			fputcsv($fp, $final_row);
		}
	}



}
?>