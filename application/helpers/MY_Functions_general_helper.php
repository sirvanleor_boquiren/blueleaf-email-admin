<?php

function check_login($session_var)
{
	if(isset($session_var->userdata['theblueleaf_logged_in']) 
		&& $session_var->userdata['theblueleaf_logged_in'] == THEBLUELEAF_SESSION_KEY)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_type_desc($blueleaf_type)
{
	$blueleaf_types = array(
		"1" => "PAVILION",
		"2" => "FILIPINAS",
		"3" => "COSMOPOLITAN"
		);

	return $blueleaf_types[$blueleaf_type];
}

function get_type_full_desc($blueleaf_type)
{
	$blueleaf_types = array(
		"0" => "Super Admin",
		"1" => "The Blue Leaf Events Pavilion",
		"2" => "The Blue Leaf Filipinas",
		"3" => "The Blue Leaf Cosmopolitan"
		);

	return $blueleaf_types[$blueleaf_type];
}

function get_all_venues()
{
	$blueleaf_venues = array(
		"1" => "The Blue Leaf Events Pavilion",
		"2" => "The Blue Leaf Filipinas",
		"3" => "The Blue Leaf Cosmopolitan"
		);
	return $blueleaf_venues;
}

function concat_existing_get()
{
	$pagination_a_href = "";
	$string_get = "";
	if(isset($_GET))
	{
	  $existing_get = array();
	  foreach ($_GET as $get_key => $get_value) {
	    $existing_get[] = $get_key."=".$get_value;
	    $string_get = implode("&", $existing_get);
	  }
	}
	if($string_get != "")
	{
		$pagination_a_href = "?".$string_get;
	}
	
	return $pagination_a_href;
}

function require_tcpdf()
{
	require_once(APPPATH.'helpers/tcpdf/tcpdf.php');
}
	
?>