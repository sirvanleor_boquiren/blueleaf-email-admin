<?php

class Email_model extends CI_model
{
	public function __construct()
	{
		# ...
	}

	# CMS	

	public function getAllEmails($is_reports = false) # $is_reports is when called for exporting data, which outputs no limit
	{
		$this->db->from("emails");
		$this->db->order_by("date_sent", "desc");
		if($this->session->userdata['is_super'] != 1)
		{
			$this->db->where("type", $this->session->userdata['type']);
		}

		// --------------------------------------------------------------------------------
		//	Optimize, but save this code for future optimization once filters becomes more than 3 
		//
		// $where = "";
		// $get_var = array(
		// 	"from" => "date_sent >= ",
		// 	"to" => "date_sent <= ",
		// 	"type" => "type = "
		// 	);

		// $where_arr = array();
		// foreach ($get_var as $get_key => $get_value) {
		// 	if(isset($_GET[$get_key]) && $_GET[$get_key] != "")
		// 	{
		// 		if($get_key == "from")
		// 		{
		// 			$_GET[$get_key] = date("Y-m-d H:i:s", strtotime($_GET[$get_key]));
		// 		}
		// 		if($get_key == "to")
		// 		{
		// 			$_GET[$get_key] = date("Y-m-d H:i:s", strtotime($_GET[$get_key]));	
		// 		}
		// 		$where_arr[] = $get_value."'".$_GET[$get_key]."'";
		// 	}
		// }
		// --------------------------------------------------------------------------------

		# Check for filters
		$where_arr = array();
		if(isset($_GET['from']) && $_GET['from'] != "")
		{
			$where_arr[] = "date_sent >= '".date("Y-m-d H:i:s", strtotime($_GET['from']))."'";
		}
		if(isset($_GET['to']) && $_GET['to'] != "")
		{
			$where_arr[] = "date_sent <= '".date("Y-m-d 23:59:59", strtotime($_GET['to']))."'";
		}

		$where = implode(" AND ", $where_arr);
		if($where != "")
		{
			$this->db->where($where);
		}
		
		if(!$is_reports)
		{
			# Pagination set limit
			$limit = 0;
			if($this->uri->segment(3) !== FALSE)
			{
				$limit = $this->uri->segment(3);
			}
			$this->db->limit(TABLE_DATA_PER_PAGE, $limit); # number_per_page, start row
		}

		$db_emails = $this->db->get();
		if($db_emails->num_rows() > 0)
		{
			return $db_emails->result();
		}
		else
		{
			return false;
		}
	}

	public function getTotalEmails()
	{
		$this->db->select("e_id");
		$this->db->from("emails");

		# Check for filters
		$where = "";
		$get_var = array(
			"from" => "date_sent >= ",
			"to" => "date_sent <= ",
			"type" => "type = "
			);

		$where_arr = array();
		foreach ($get_var as $get_key => $get_value) {
			if(isset($_GET[$get_key]) && $_GET[$get_key] != "")
			{
				if($get_key == "from" || $get_key == "to")
				{
					$_GET[$get_key] = date("Y-m-d H:i:s", strtotime($_GET[$get_key]));
				}
				$where_arr[] = $get_value."'".$_GET[$get_key]."'";
			}
		}

		$where = implode(" AND ", $where_arr);
		if($where != "")
		{
			$this->db->where($where);
		}

		return $this->db->get()->num_rows();
	}
}

?>