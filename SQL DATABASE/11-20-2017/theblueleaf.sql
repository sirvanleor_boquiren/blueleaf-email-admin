-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2017 at 06:38 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `theblueleaf`
--

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
`e_id` int(12) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 - Pavilion, 2 - filipinas, 3 - cosmopolitan',
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `landline` varchar(150) NOT NULL,
  `mobile` varchar(150) NOT NULL,
  `venue` varchar(250) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(100) NOT NULL,
  `guest_num` varchar(10) NOT NULL,
  `event_type` varchar(100) NOT NULL,
  `message` varchar(300) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`e_id`, `type`, `name`, `email`, `landline`, `mobile`, `venue`, `event_date`, `event_time`, `guest_num`, `event_type`, `message`, `date_sent`) VALUES
(1, 1, 'Test Email 1', 'email1@test.com', '123-12341', '09163232293', 'The Blue Leaf Events Pavilion', '2017-11-10', '', '100', 'Others', 'Hi there!\r\n\r\nPlease test this shit! hahahahahaha.\r\nHello\r\nThere\r\nHi\r\nI am Van\r\n\r\nThanks.', '2017-05-01 03:17:12'),
(2, 2, 'Test Email 2', 'email2@test.com', '123-12341', '09163232293', 'The Blue Leaf Filipinas', '2017-12-07', '', '90', 'Others', 'Hi there!\r\n\r\nPlease test this shit! hahahahahaha.\r\nHello\r\nThere\r\nHi\r\nI am Van\r\n\r\nThanks.', '2017-07-31 03:17:12'),
(3, 3, 'Test Email 3', 'email3@test.com', '123-12341', '09163232296', 'The Blue Leaf Cosmopolitan', '2017-12-28', '', '67', 'Others', 'Hi there!\r\n\r\nPlease test this shit! hahahahahaha.\r\nHello\r\nThere\r\nHi\r\nI am Van\r\n\r\nThanks.', '2017-06-20 03:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`u_id` int(12) NOT NULL,
  `fullname` varchar(225) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 - events pavilion, 2 - filipinas, 3 - cosmopolitan',
  `is_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - admin, 1 - super admin',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - false, 1 - true'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `fullname`, `email`, `password`, `type`, `is_super`, `date_added`, `is_deleted`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$N6NfBJVNEPy8JXEtRFGaeO4IBjX7uhIcyu2NOqhr60/jgpvjj5hEG', 0, 1, '2017-07-31 03:09:49', 0),
(2, 'TestPavillion', 'admin@pavillion.com', '$2y$10$PgDhKt0Wrwsl6cX4Z.kSJ.JZDozs0EnT7rpHqCP23tbR9ZDFBKrU6', 1, 0, '2017-07-31 08:44:37', 0),
(3, 'TestFilipinas', 'admin@filipinas.com', '$2y$10$rdUNvhhOdLhBnA8jNEl5P.8BnRhb5ukt3jcAeRsbs.8nliB.1MUje', 2, 0, '2017-07-31 08:45:26', 0),
(4, 'TestCosmopolitan', 'admin@cosmopolitan.com', '$2y$10$u3KjMXd2WQZV1d0dfAuI9.Yzbyp9QKrBIFIT2n8T4lLLnCyOmjy2S', 3, 0, '2017-07-31 08:45:48', 0),
(5, 'super', 'admin@super.com', '$2y$10$7QXg/PUOOlBzZmI.jCmLm.katXLQUaa4tSgB6yZMRDGTXzN3zmDHW', 0, 1, '2017-08-03 06:10:30', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
 ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
MODIFY `e_id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `u_id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
